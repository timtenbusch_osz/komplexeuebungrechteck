package test;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck eck0 = new Rechteck();
		eck0.setX(10);
		eck0.setY(10);
		eck0.setBreite(30);
		eck0.setHoehe(40);
		Rechteck eck1 = new Rechteck();
		eck1.setX(25);
		eck1.setY(25);
		eck1.setBreite(100);
		eck1.setHoehe(20);
		Rechteck eck2 = new Rechteck();
		eck2.setX(260);
		eck2.setY(10);
		eck2.setBreite(200);
		eck2.setHoehe(100);
		Rechteck eck3 = new Rechteck();
		eck3.setX(5);
		eck3.setY(500);
		eck3.setBreite(300);
		eck3.setHoehe(25);
		Rechteck eck4 = new Rechteck();
		eck4.setX(100);
		eck4.setY(100);
		eck4.setBreite(100);
		eck4.setHoehe(100);
		Rechteck eck5 = new Rechteck(200,200,200,200);
		Rechteck eck6 = new Rechteck(800,400,20,20);
		Rechteck eck7 = new Rechteck(800,450,20,20);
		Rechteck eck8 = new Rechteck(850,400,20,20);
		Rechteck eck9 = new Rechteck(855,455,25,25);
		
		//Pr�fung, ob toString Methode funktioniert
		System.out.println("toString() funktioniert: " + eck0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		//Pr�fen ob Rechteckerstellung mit negativen Breiten und H�hen m�glich ist
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
		System.out.println(eck10);
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println(eck11);
		//Controllerobjekt erstellen und testen
		BunteRechteckeController brc = new BunteRechteckeController();
		brc.add(eck0);
		brc.add(eck1);
		brc.add(eck2);
		brc.add(eck3);
		brc.add(eck4);
		brc.add(eck5);
		brc.add(eck6);
		brc.add(eck7);
		brc.add(eck8);
		brc.add(eck9);
		System.out.println(brc);
		
		Rechteck eck12 = Rechteck.generiereZufallsRechteck();
		System.out.println(eck12);
		
		rechteckeTesten();
	}

	private static void rechteckeTesten() {
		boolean test = true;
		Rechteck[] rechtecke = new Rechteck[50000];
		Rechteck referenz = new Rechteck(0,0,1200,1000);
		for(int i = 0; i < rechtecke.length; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
			if(!referenz.enthaelt(rechtecke[i])) {
				System.out.println(rechtecke[i]);
				test = false;
			}
		}
		
		System.out.println("Rechteckgenerierungstest bestanden: " + test);
	}
	
}
