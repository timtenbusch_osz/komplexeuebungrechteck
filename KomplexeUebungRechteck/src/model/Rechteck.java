package model;

import java.util.Random;

public class Rechteck {
	private Punkt p;
	private int breite;
	private int hoehe;
	private static Random rand = new Random();
	public static final int MAXBREITE = 1200;
	public static final int MAXHOEHE = 1000;

	public Rechteck() {
		this.p = new Punkt();
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		super();
		this.p = new Punkt(x,y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	public int getX() {
		return this.p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}

	public boolean enthaelt(Punkt p) {
		return enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(int x, int y) {
		return this.p.getX() <= x && x <= this.p.getX() + this.breite &&
				this.p.getY() <= y && y <= this.p.getY() + this.hoehe;
	}

	public boolean enthaelt(Rechteck rechteck) {
		Punkt linksoben = new Punkt(rechteck.getX(), rechteck.getY());
		Punkt rechtsunten = new Punkt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
		return enthaelt(linksoben) && enthaelt(rechtsunten);
	}

	public static Rechteck generiereZufallsRechteck(){
		int x  =rand.nextInt(MAXBREITE+1);
		int y = rand.nextInt(MAXHOEHE+1);
		int breite = rand.nextInt(MAXBREITE-x+1);
		int hoehe = rand.nextInt(MAXHOEHE-y+1);
		return new Rechteck(x, y, breite, hoehe);
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + this.p.getX() + ", y=" + this.p.getY() + ", breite=" + breite + ", hoehe=" + hoehe
				+ "]";
	}
}
