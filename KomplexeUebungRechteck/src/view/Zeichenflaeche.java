package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;

@SuppressWarnings("serial")
public class Zeichenflaeche extends JPanel {

	private BunteRechteckeController brc;

	public Zeichenflaeche(BunteRechteckeController brc) {
		this.brc = brc;
	}

	@Override
	public void paintComponent(Graphics g) {
		//Rechtecke zeichnen
		g.setColor(Color.BLACK);
		for (Rechteck r : brc.getRechtecke()) {
			g.fillRect(r.getX(), r.getY(), r.getBreite(), r.getHoehe());
		}

	}
}
