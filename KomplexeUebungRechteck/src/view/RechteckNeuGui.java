package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

public class RechteckNeuGui extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfdX;
	private JTextField tfdY;
	private JTextField tfdLaenge;
	private JTextField tfdHoehe;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RechteckNeuGui frame = new RechteckNeuGui(new BunteRechteckeController());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckNeuGui(BunteRechteckeController brc) {
		this.brc = brc;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel pnlEingabe = new JPanel();
		contentPane.add(pnlEingabe, BorderLayout.CENTER);
		pnlEingabe.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lblX = new JLabel("X");
		pnlEingabe.add(lblX);
		
		tfdX = new JTextField();
		pnlEingabe.add(tfdX);
		tfdX.setColumns(10);
		
		JLabel lblY = new JLabel("Y");
		pnlEingabe.add(lblY);
		
		tfdY = new JTextField();
		pnlEingabe.add(tfdY);
		tfdY.setColumns(10);
		
		JLabel lblLnge = new JLabel("L\u00E4nge");
		pnlEingabe.add(lblLnge);
		
		tfdLaenge = new JTextField();
		pnlEingabe.add(tfdLaenge);
		tfdLaenge.setColumns(10);
		
		JLabel lblHoehe = new JLabel("H\u00F6he");
		pnlEingabe.add(lblHoehe);
		
		tfdHoehe = new JTextField();
		pnlEingabe.add(tfdHoehe);
		tfdHoehe.setColumns(10);
		
		JButton btnSpeichern = new JButton("speichern");
		pnlEingabe.add(btnSpeichern);
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonSpeichernClicked();
			}
		});
		setVisible(true);
	}

	protected void buttonSpeichernClicked() {
		int x = Integer.parseInt(tfdX.getText());
		int y = Integer.parseInt(tfdY.getText());
		int laenge = Integer.parseInt(tfdLaenge.getText());
		int hoehe = Integer.parseInt(tfdHoehe.getText());
		Rechteck r = new Rechteck(x,y, laenge, hoehe);
		brc.add(r);
	}

}
